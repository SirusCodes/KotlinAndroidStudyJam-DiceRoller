# Android Study Jam - Adding Sensor Input to an App

Starter Code: [`google-developer-training/android-basics-kotlin-create-dice-roller-with-button-app-solution`](https://github.com/google-developer-training/android-basics-kotlin-create-dice-roller-with-button-app-solution)


## Description
This repository contains the finished code for the Android Study Jam session about adding sensor inputs to an app, using Kotlin.

## App description
The standard dice-roller app is extended to trigger the roll if the device is shaken.

## Build Instructions
### Android Studio
- Click `Get from Version Control`
- Enter the repository url
- Wait for project sync to finish in Android Studio
- Click on `Run 'app'` to build and run the app on an emulator or physical device

### Command Line
- Clone the project
- Start the emulator or connect a physical device
- Run:
  - `gradlew assembleDebug` to build the apk
  - `adb install app/build/outputs/apk/debug/app-debug.apk` to install the apk